package pl.pragmatists.legacycode.eclipse._1extractmethod;

import java.util.HashMap;
import java.util.Map;

public class CodeAnalyzer {

    private Map<Integer, Integer> lineLengths = new HashMap<Integer, Integer>();
    private int lineCount;
    @SuppressWarnings("unused")
    private int totalChars;
    private int widestLineLength;
    @SuppressWarnings("unused")
    private int widestLineNumber;
    private Map<String, Integer> classStats;

    public void measureLine(String line) {
        lineCount++;
        totalChars += line.length();

        // TODO: Extract this block of code to method with line length as parameter
        // Try two ways:
        // 1) extract parameter to local variable, then extract method
        // 2) extract method first, then add parameter
        // START/////////////////////////////////////////////////////////////
        if (!lineLengths.containsKey(line.length())) {
            lineLengths.put(line.length(), 1);
        } else {
            int currentCount = lineLengths.get(line.length());
            lineLengths.put(line.length(), ++currentCount);
        }
        // END///////////////////////////////////////////////////////////////

        if (line.length() > widestLineLength) {
            widestLineNumber = lineCount;
            widestLineLength = line.length();
        }
    }

    // TODO: Extract duplicated code
    public void classTypesDensity(String line, int increase) {
        classStats.put("abstract per file", count("abstract class") / filesCount());
        classStats.put("public per file", count("public class") / filesCount());
    }

    private int filesCount() {
        return 0;
    }

    private int count(String query) {
        return 0;
    }

}
