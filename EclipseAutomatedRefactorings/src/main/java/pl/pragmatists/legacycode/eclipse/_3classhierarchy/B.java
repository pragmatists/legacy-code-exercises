package pl.pragmatists.legacycode.eclipse._3classhierarchy;

public class B {

    public String callMe() {
        double interestingValue = commonMethod();
        double stuffDone = doMyStuff(interestingValue);
        return Double.toString(stuffDone);
    }

    private double doMyStuff(double value) {
        return Math.sin(value);
    }

    private double commonMethod() {
        return Math.PI;
    }
}
