package pl.pragmatists.legacycode.eclipse._5parameterobject;

import java.util.Date;
import java.util.List;

public class TripPlanner {

    // TODO: Introduce DateSpan class

    @SuppressWarnings("unused")
    public class Trip {

        private Date from;
        private Date to;

        public Trip(Date from, Date to) {
            this.from = from;
            this.to = to;
        }

    }

    public class Coach {

    }

    public List<Coach> findCoaches(Date from, Date to) {
        return null;
    }

    public Trip createTrip(Date from, Date to) {
        return new Trip(from, to);
    }

}
