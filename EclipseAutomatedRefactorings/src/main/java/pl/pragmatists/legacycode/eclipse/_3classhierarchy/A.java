package pl.pragmatists.legacycode.eclipse._3classhierarchy;

public class A {

    public String callMe() {
        double interestingValue = commonMethod();
        double stuffDone = doMyStuff(interestingValue);
        return Double.toString(stuffDone);
    }

    private double doMyStuff(double value) {
        return Math.floor(value);
    }

    private double commonMethod() {
        return Math.PI;
    }
}
