package pl.pragmatists.legacycode.eclipse._4creationmethods;

import java.util.Date;

public class Loan {

    public class CapitalStrategy {

    }

    // TODO: replace constructors with meaningful creation methods

    // term loan
    public Loan(int commitment, int riskRating, Date maturity) {

    }

    // revolving loan
    public Loan(int commitment, int riskRating, Date maturity, Date expiry) {

    }

    // term loan
    public Loan(CapitalStrategy capitalStrategy, int riskRating, int maturity) {

    }

    // revolving loan
    public Loan(CapitalStrategy capitalStrategy, int riskRating, int maturity, Date expiry) {

    }
}
