package pl.pragmatists.legacycode.eclipse._2extractclass;

import java.util.HashMap;
import java.util.Map;

public class CodeAnalyzer {

    private int lineCount;
    private int totalChars;
    // TODO: Extract LineLengthHistogram class
    private Map<Integer, Integer> lineLengths = new HashMap<Integer, Integer>();
    private int widestLineLength;
    private int widestLineNumber;

    public void measureLine(String line) {
        updateTotalLines();
        updateTotalChars(line.length());
        updateHistogram(line.length());
        recordWidest(line.length());
    }

    private void recordWidest(int lineLength) {
        if (lineLength > widestLineLength) {
            widestLineNumber = lineCount;
            widestLineLength = lineLength;
        }
    }

    private void updateHistogram(int lineLength) {
        if (notInHistogram(lineLength)) {
            putFirst(lineLength);
        } else {
            increaseCount(lineLength);
        }
    }

    private void increaseCount(int lineLength) {
        int currentCount = lineLengths.get(lineLength);
        lineLengths.put(lineLength, ++currentCount);
    }

    private void putFirst(int lineLength) {
        lineLengths.put(lineLength, 1);
    }

    private boolean notInHistogram(int lineLength) {
        return !lineLengths.containsKey(lineLength);
    }

    private void updateTotalChars(int lineLength) {
        totalChars += lineLength;
    }

    private void updateTotalLines() {
        lineCount++;
    }

    public int getLineCount() {
        return lineCount;
    }

    public int getMeanLineWidth() {
        return totalChars / lineCount;
    }

    public int lineCountForWidth(int width) {
        if (notInHistogram(width)) {
            return 0;
        }
        return lineLengths.get(width);
    }

    public int getWidestLineNumber() {
        return widestLineNumber;
    }

    public int getWidestLineLength() {
        return widestLineLength;
    }

}
